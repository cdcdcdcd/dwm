/* Constants */
#define TERMINAL "urxvt"
#define TERMCLASS "St"

/* appearance */
static unsigned int borderpx  = 3;        /* border pixel of windows */
static unsigned int snap      = 32;       /* snap pixel */
static unsigned int gappih    = 20;       /* horiz inner gap between windows */
static unsigned int gappiv    = 10;       /* vert inner gap between windows */
static unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
static int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static char *fonts[]          = { "monospace:size=14", "JoyPixels:pixelsize=13:antialias=true:autohint=true"}; //ttf-inconsolata:size=14
static char font[]            = "monospace:size=10";
static char dmenufont[]       = "monospace:size=10";
                                                                                                                     
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee"; //+eeeeee"
static char selbordercolor[]        = "#D49965"; //#770000
static char selbgcolor[]            = "#93583E";

static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {TERMINAL, "-n", "spterm", "-g", "120x34", NULL };
const char *spcmd2[] = {TERMINAL, "-n", "spcalc", "-f", "monospace:size=16", "-g", "50x20", "-e", "bc", "-lq", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spcalc",      spcmd2},
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "w", "e", "a", "s", "d" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	*/
	/* class    instance      title       	 tags mask    isfloating   isterminal  noswallow  monitor */
	{ "nheko",     NULL,       NULL,       	    1 << 0,       0,           0,         0,        -1 },
	{ "LibreWolf", NULL,       NULL,       	    1 << 1,       0,           0,         0,        -1 },
	{ "Brave-browser", NULL,       NULL,       	    1 << 1,       0,           0,         0,        -1 },
	{ "URxvt",     NULL,       NULL,    	    0,            1,           0,         0,        -1 },
	/* { "St",        NULL,       "float",    	    1 << 2,       0,           0,         0,        -1 }, */
	{ "Emacs",     NULL,       NULL,       	    1 << 4,       0,           0,         0,        -1 },
	{ "Anki",      NULL,       NULL,            1 << 5,       0,           0,         0,        -1 },
	{ "firefox",   NULL,       NULL,            1 << 1,       0,           0,         0,        -1 },
	{ "qBittorrent", NULL,       NULL,          1 << 8,     0,           0,         0,        -1 },
	{ "Deadbeef", NULL,       NULL,          1 << 7,     0,           0,         0,        -1 },
	 /*{ "Nsxiv",     NULL,      "float",             1 << 8,      1,           0,         0,        -1 }, */
	{ TERMCLASS,   NULL,       NULL,       	    0,            0,           1,         0,        -1 },
	{ NULL,       NULL,        "float",         0,            1,           0,         0,        -1 },
	{ NULL,       NULL,        "tag3",          1 << 2,       0,           0,         0,        -1 }, /*doesnt work*/
	{ NULL,       NULL,       "Event Tester",   0,            0,           0,         1,        -1 },
	{ NULL,      "spterm",    NULL,       	    SPTAG(0),     1,           1,         0,        -1 },
	{ NULL,      "spcalc",    NULL,       	    SPTAG(1),     1,           1,         0,        -1 },
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
 	{ "[]=",	tile },			/* Default: Master on left, slaves on right */
	{ "><>",	NULL },			/* no layout function means floating behavior */
	{ "TTT",	bstack },		/* Master on top, slaves on bottom */

	{ "[@]",	spiral },		/* Fibonacci spiral */
	{ "[\\]",	dwindle },		/* Decreasing in size right and leftward */

	{ "[D]",	deck },			/* Master on left, slaves in monocle-like mode on right */
 	{ "[M]",	monocle },		/* All windows on top of eachother */

	{ "|M|",	centeredmaster },		/* Master in middle, slaves on sides */
	{ ">M>",	centeredfloatingmaster },	/* Same but master floats */

	{ NULL,		NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define MODKEY2 Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY2,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY2|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY2|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY2|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
	{ MOD,	XK_k,	ACTION##stack,	{.i = INC(+1) } }, \
	{ MOD,	XK_i,	ACTION##stack,	{.i = INC(-1) } }, \
	/*{ MOD,  XK_v,   ACTION##stack,  {.i = 0 } }, \*/
	/* { MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \ */
	/* { MOD, XK_a,     ACTION##stack, {.i = 1 } }, \ */
	/* { MOD, XK_z,     ACTION##stack, {.i = 2 } }, \ */
	/* { MOD, XK_x,     ACTION##stack, {.i = -1 } }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "1"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { TERMINAL, NULL };

static const char *dwmfifo = "/tmp/dwm.fifo";
static Command commands[] = {
	/* { "term",            spawn,          {.v = termcmd} }, */
	{ "quit",            quit,           {0} },
	{ "loadres",         set_col,{0} },
	{ "togglebar",       togglebar,      {0} },
	{ "togglewarp",      togglewarp,     {0} },
	{ "focusstack+",     focusstack,     {.i = +1} },
	{ "focusstack-",     focusstack,     {.i = -1} },
	{ "incnmaster+",     incnmaster,     {.i = +1} },
	{ "incnmaster-",     incnmaster,     {.i = -1} },
	{ "setmfact+",       setmfact,       {.f = +0.05} },
	{ "setmfact-",       setmfact,       {.f = -0.05} },
	{ "zoom",            zoom,           {0} },
	{ "view",            view,           {0} },
	{ "killclient",      killclient,     {0} },
	{ "setlayout-tiled", setlayout,      {.v = &layouts[0]} },
	{ "setlayout-bstack",  setlayout,      {.v = &layouts[2]} },
	{ "setlayout-fib",  setlayout,      {.v = &layouts[3]} },
	{ "setlayout-dwindle",  setlayout,      {.v = &layouts[4]} },
	{ "setlayout-deck",  setlayout,      {.v = &layouts[5]} },
	{ "setlayout-monocle",  setlayout,      {.v = &layouts[6]} },
	{ "setlayout-cmaster",  setlayout,      {.v = &layouts[7]} },
	{ "setlayout-fmaster",  setlayout,      {.v = &layouts[8]} },
	{ "setlayout-float", setlayout,      {.v = &layouts[1]} },
	{ "togglelayout",    setlayout,      {0} },
	{ "togglefloating",  togglefloating, {0} },
	{ "viewall",         view,           {.ui = ~0} },
	{ "tag",             tag,            {.ui = ~0} },
	{ "focusmon+",       focusmon,       {.i = +1} },
	{ "focusmon-",       focusmon,       {.i = -1} },
	{ "tagmon+",         tagmon,         {.i = +1} },
	{ "tagmon-",         tagmon,         {.i = -1} },
	{ "view1",           view,           {.ui = 1 << 0} },
	{ "view2",           view,           {.ui = 1 << 1} },
	{ "view3",           view,           {.ui = 1 << 2} },
	{ "view4",           view,           {.ui = 1 << 3} },
	{ "view5",           view,           {.ui = 1 << 4} },
	{ "view6",           view,           {.ui = 1 << 5} },
	{ "view7",           view,           {.ui = 1 << 6} },
	{ "view8",           view,           {.ui = 1 << 7} },
	{ "view9",           view,           {.ui = 1 << 8} },
	{ "toggleview1",     toggleview,     {.ui = 1 << 0} },
	{ "toggleview2",     toggleview,     {.ui = 1 << 1} },
	{ "toggleview3",     toggleview,     {.ui = 1 << 2} },
	{ "toggleview4",     toggleview,     {.ui = 1 << 3} },
	{ "toggleview5",     toggleview,     {.ui = 1 << 4} },
	{ "toggleview6",     toggleview,     {.ui = 1 << 5} },
	{ "toggleview7",     toggleview,     {.ui = 1 << 6} },
	{ "toggleview8",     toggleview,     {.ui = 1 << 7} },
	{ "toggleview9",     toggleview,     {.ui = 1 << 8} },
	{ "tag1",            tag,            {.ui = 1 << 0} },
	{ "tag2",            tag,            {.ui = 1 << 1} },
	{ "tag3",            tag,            {.ui = 1 << 2} },
	{ "tag4",            tag,            {.ui = 1 << 3} },
	{ "tag5",            tag,            {.ui = 1 << 4} },
	{ "tag6",            tag,            {.ui = 1 << 5} },
	{ "tag7",            tag,            {.ui = 1 << 6} },
	{ "tag8",            tag,            {.ui = 1 << 7} },
	{ "tag9",            tag,            {.ui = 1 << 8} },
	{ "toggletag1",      toggletag,      {.ui = 1 << 0} },
	{ "toggletag2",      toggletag,      {.ui = 1 << 1} },
	{ "toggletag3",      toggletag,      {.ui = 1 << 2} },
	{ "toggletag4",      toggletag,      {.ui = 1 << 3} },
	{ "toggletag5",      toggletag,      {.ui = 1 << 4} },
	{ "toggletag6",      toggletag,      {.ui = 1 << 5} },
	{ "toggletag7",      toggletag,      {.ui = 1 << 6} },
	{ "toggletag8",      toggletag,      {.ui = 1 << 7} },
	{ "toggletag9",      toggletag,      {.ui = 1 << 8} },
};

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "normbordercolor",		STRING,	&normbordercolor },
		{ "selbordercolor",		STRING,	&selbordercolor },
		{ "normbgcolor",		STRING,	&normbgcolor },
		{ "normfgcolor",		STRING,	&normfgcolor },
		{ "selfgcolor",		STRING,	&selfgcolor },
		{ "selbgcolor",		STRING,	&selbgcolor },
		{ "borderpx",		INTEGER, &borderpx },
		{ "snap",		INTEGER, &snap },
		{ "showbar",		INTEGER, &showbar },
		{ "topbar",		INTEGER, &topbar },
		{ "nmaster",		INTEGER, &nmaster },
		{ "resizehints",	INTEGER, &resizehints },
		{ "mfact",		FLOAT,	&mfact },
		{ "gappih",		INTEGER, &gappih },
		{ "gappiv",		INTEGER, &gappiv },
		{ "gappoh",		INTEGER, &gappoh },
		{ "gappov",		INTEGER, &gappov },
		{ "swallowfloating",	INTEGER, &swallowfloating }, 
		{ "smartgaps",		INTEGER, &smartgaps }, 
}; 

#include <X11/XF86keysym.h>
#include "shiftview.c"

static Key keys[] = {
	/* modifier                     key        function        argument */
	STACKKEYS(MODKEY2,                          focus)
	STACKKEYS(MODKEY,                           push)
	{ MODKEY,			XK_grave,	spawn,	SHCMD("dmenuunicode") },
	TAGKEYS(			XK_1,		0)
	TAGKEYS(			XK_2,		1)
	TAGKEYS(			XK_3,		2)
	TAGKEYS(			XK_4,		3)
	TAGKEYS(			XK_w,		4)
	TAGKEYS(			XK_e,		5)
	TAGKEYS(			XK_a,		6)
	TAGKEYS(			XK_s,		7)
	TAGKEYS(			XK_d,		8)
	{ MODKEY,			XK_0,		view,		{.ui = ~0 } },
	{ MODKEY|ShiftMask,	XK_0,		tag,		{.ui = ~0 } },
	/* { NULL,			    XK_F2,		view,		{0} }, */
	{ MODKEY2,			XK_Tab,		view,		{0} },
	{ NULL,             XK_F12,		killclient,	{0} },
	{ MODKEY|ShiftMask,	XK_s,		setlayout,	{.v = &layouts[0]} }, /* tile */
	{ MODKEY,			XK_o,		incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,	XK_o,		incnmaster,     {.i = -1 } },
	{ MODKEY,			XK_a,		togglegaps,	{0} },
	{ MODKEY,			XK_s,		togglesticky,	{0} },
	//{ MODKEY2,			XK_q,		spawn,          SHCMD("dmenu_run") },
	{ MODKEY2,			XK_q,		spawn,          {.v = dmenucmd} },
	{ NULL,	            XK_F1,		togglefullscr,	{0} },
	{ MODKEY|ShiftMask,	XK_d,		setlayout,	{.v = &layouts[8]} },
	{ MODKEY,			XK_g,		shiftview,	{ .i = -1 } },
	{ MODKEY|ShiftMask,	XK_g,		shifttag,	{ .i = -1 } },
	/* J and K are automatically bound above in STACKEYS */
	{ MODKEY2,			XK_j,		setmfact,	    {.f = -0.05} },
	{ MODKEY2,			XK_l,		setmfact,      	{.f = +0.05} },
	{ MODKEY,			XK_semicolon,	shiftview,	{ .i = 1 } },
	{ MODKEY|ShiftMask,	XK_semicolon,	shifttag,	{ .i = 1 } },
	/* { MODKEY,			XK_i,	togglescratch,	{.ui = 1} }, */
	/* { MODKEY|ShiftMask,		XK_i,	togglescratch,	{.ui = 0} }, */
	{ MODKEY,			XK_u,		incrgaps,	{.i = +3 } },
	{ MODKEY|ShiftMask,	XK_u,		incrgaps,	{.i = -3 } },
	{ MODKEY,			XK_h,		togglebar,	{0} },
	/* { MODKEY,			XK_n,		spawn,		SHCMD(TERMINAL " -e nvim -c VimwikiIndex") }, */
	{ MODKEY2,			XK_period,	focusmon,	{.i = -1 } },
	{ MODKEY2|ShiftMask,XK_period,	tagmon,		{.i = -1 } },
	{ MODKEY,			XK_Page_Up,	shiftview,	{ .i = -1 } },
	{ MODKEY|ShiftMask,	XK_Page_Up,	shifttag,	{ .i = -1 } },
	{ MODKEY,			XK_Page_Down,	shiftview,	{ .i = +1 } },
	{ MODKEY|ShiftMask,	XK_Page_Down,	shifttag,	{ .i = +1 } },
	{ MODKEY2,			XK_Return,	zoom,		{0} },
	{ MODKEY,		    XK_space,	togglefloating,	{0} },
	//{ MODKEY,			XK_Scroll_Lock,	spawn,		SHCMD("killall screenkey || screenkey &") },
	{ 0, XF86XK_PowerOff,		spawn,		SHCMD("sysact") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
 	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} }, /*fifo*/
	{ ClkStatusText,        ShiftMask,      Button3,        spawn,          SHCMD(TERMINAL " -e nvim ~/.local/src/dwmblocks/config.h") },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        defaultgaps,	{0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkClientWin,		MODKEY,		Button4,	incrgaps,	{.i = +1} },
	{ ClkClientWin,		MODKEY,		Button5,	incrgaps,	{.i = -1} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkTagBar,		0,		Button4,	shiftview,	{.i = -1} },
	{ ClkTagBar,		0,		Button5,	shiftview,	{.i = 1} },
	{ ClkRootWin,		0,		Button2,	togglebar,	{0} },
};

/* trash */
	/* { MODKEY|ShiftMask,		XK_t,		setlayout,	{.v = &layouts[1]} }, /\* bstack *\/ */
	/* { MODKEY|ShiftMask,		XK_z,		setlayout,	{.v = &layouts[3]} }, /\* dwindle *\/ */
	/* { MODKEY,			XK_u,		setlayout,	{.v = &layouts[4]} }, /\* deck *\/ */
	/* { MODKEY|ShiftMask,		XK_u,		setlayout,	{.v = &layouts[5]} }, /\* monocle *\/ */
	/* { MODKEY,			XK_i,		setlayout,	{.v = &layouts[6]} }, /\* centeredmaster *\/ */
	/* { MODKEY|ShiftMask,		XK_i,		setlayout,	{.v = &layouts[7]} }, /\* centeredfloatingmaster *\/ */
	/* { 0, XF86XK_Calculator,		spawn,		SHCMD(TERMINAL " -e bc -l") },  */
	/* { 0, XF86XK_MonBrightnessUp,	spawn,		SHCMD("xbacklight -inc 15") }, */
	/* { 0, XF86XK_MonBrightnessDown,	spawn,		SHCMD("xbacklight -dec 15") }, */
	/* { MODKEY,			XK_BackSpace,	spawn,		SHCMD("sysact") }, */
	/* { MODKEY|ShiftMask,		XK_BackSpace,	spawn,		SHCMD("sysact") }, */
	/* { MODKEY|ShiftMask,		XK_Tab,		spawn,		SHCMD("") }, */
	/*{ MODKEY,			XK_p,			spawn,		SHCMD("mpc --port 6601 toggle") }, */
	/*{ MODKEY|ShiftMask,		XK_q,		spawn,		SHCMD("sysact") },*/
	/* { MODKEY|ShiftMask,		XK_Escape,	spawn,	SHCMD("") }, */
	/*{ MODKEY,			XK_w,		spawn,		SHCMD("$BROWSER") },*/
	/* { MODKEY|ShiftMask,		XK_grave,	togglescratch,	SHCMD("") }, */
	/*{ MODKEY|ShiftMask,		XK_w,		spawn,		SHCMD(TERMINAL " -e sudo nmtui") },*/
	/*{ MODKEY,			XK_e,		spawn,		SHCMD(TERMINAL " -e neomutt ; pkill -RTMIN+12 dwmblocks; rmdir ~/.abook") },*/
	/*{ MODKEY|ShiftMask,		XK_e,		spawn,		SHCMD(TERMINAL " -e abook -C ~/.config/abook/abookrc --datafile ~/.config/abook/addressbook") },*/
	/*{ MODKEY,			XK_r,		spawn,		SHCMD(TERMINAL " -e lf") }, */
	/*{ MODKEY|ShiftMask,		XK_r,		spawn,		SHCMD(TERMINAL " -e htop") },*/
	/* { MODKEY|ShiftMask,		XK_backslash,		spawn,		SHCMD("") }, */
	/*{ MODKEY|ShiftMask,		XK_p,			spawn,		SHCMD("mpc pause ; pauseallmpv") }, */
	/*{ MODKEY,			XK_z,		setlayout,	{.v = &layouts[2]} },*/ /* spiral */
	/*{ MODKEY,			XK_Left,		spawn,		SHCMD("mpc --port 6601 seek -10") },
	{ MODKEY|ShiftMask,		XK_Left,		spawn,		SHCMD("mpc --port 6601 seek -60") },
	{ MODKEY,			XK_Right,	spawn,		SHCMD("mpc --port 6601 seek +10") },
	{ MODKEY|ShiftMask,		XK_Right,	spawn,		SHCMD("mpc --port 6601 seek +60") }, */
	/* { MODKEY|ShiftMask,		XK_d,		spawn,		SHCMD("passmenu") }, */
	/* { MODKEY|ShiftMask,		XK_s,		spawn,		SHCMD("") }, */
	/*{ MODKEY,			XK_minus,	spawn,		SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,		XK_minus,	spawn,		SHCMD("pamixer --allow-boost -d 15; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,			XK_equal,	spawn,		SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,		XK_equal,	spawn,		SHCMD("pamixer --allow-boost -i 15; kill -44 $(pidof dwmblocks)") },*/
	/* { MODKEY|ShiftMask,		XK_apostrophe,	spawn,		SHCMD("") }, */
	/* { MODKEY|ShiftMask,		XK_z,		spawn,		SHCMD("") }, */
	/* { MODKEY|ShiftMask,		XK_b,		spawn,		SHCMD("") }, */
	/* { MODKEY|ShiftMask,		XK_x,		spawn,		SHCMD("") }, */
	/* { MODKEY|ShiftMask,		XK_a,		defaultgaps,	{0} }, */
	/* { MODKEY,			XK_c,		spawn,		SHCMD("") }, */
	/* { MODKEY|ShiftMask,		XK_c,		spawn,		SHCMD("") }, */
	/* V is automatically bound above in STACKKEYS */
	/*{ MODKEY|ShiftMask,		XK_n,		spawn,		SHCMD(TERMINAL " -e newsboat; pkill -RTMIN+6 dwmblocks") },*/
	/*{ MODKEY2,			XK_Right,	focusmon,	{.i = +1 } },
	{ MODKEY2|ShiftMask,		XK_Right,	tagmon,		{.i = +1 } }, */
	/* { MODKEY,			XK_m,		spawn,		SHCMD(TERMINAL " -e ncmpcpp") }, */
	/* { MODKEY|ShiftMask,		XK_m,		spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") }, */
	/* { MODKEY,			XK_comma,	spawn,		SHCMD("mpc --port 6601 prev") }, */
	/* { MODKEY|ShiftMask,		XK_comma,	spawn,		SHCMD("mpc --port 6601 0%") }, */
	/* { MODKEY,			XK_period,	spawn,		SHCMD("mpc --port 6601 next") }, */
	/* { MODKEY|ShiftMask,		XK_period,	spawn,		SHCMD("mpc --port 6601 repeat") }, */
	/* { MODKEY|Mod4Mask,              XK_h,      incrgaps,       {.i = +1 } }, */
	/* { MODKEY|Mod4Mask,              XK_l,      incrgaps,       {.i = -1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_h,      incrogaps,      {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_l,      incrogaps,      {.i = -1 } }, */
	/* { MODKEY|Mod4Mask|ControlMask,  XK_h,      incrigaps,      {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ControlMask,  XK_l,      incrigaps,      {.i = -1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} }, */
	/* { MODKEY,                       XK_y,      incrihgaps,     {.i = +1 } }, */
	/* { MODKEY,                       XK_o,      incrihgaps,     {.i = -1 } }, */
	/* { MODKEY|ControlMask,           XK_y,      incrivgaps,     {.i = +1 } }, */
	/* { MODKEY|ControlMask,           XK_o,      incrivgaps,     {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,              XK_y,      incrohgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask,              XK_o,      incrohgaps,     {.i = -1 } }, */
	/* { MODKEY|ShiftMask,             XK_y,      incrovgaps,     {.i = +1 } }, */
	/* { MODKEY|ShiftMask,             XK_o,      incrovgaps,     {.i = -1 } }, */
	/*{ 0, XF86XK_Sleep,		spawn,		SHCMD("sudo -A zzz") },
	{ 0, XF86XK_WWW,		spawn,		SHCMD("$BROWSER") },
	{ 0, XF86XK_DOS,		spawn,		SHCMD(TERMINAL) },
	{ 0, XF86XK_ScreenSaver,	spawn,		SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
	{ 0, XF86XK_TaskPane,		spawn,		SHCMD(TERMINAL " -e htop") },
	{ 0, XF86XK_Mail,		spawn,		SHCMD(TERMINAL " -e neomutt ; pkill -RTMIN+12 dwmblocks") },
	{ 0, XF86XK_MyComputer,		spawn,		SHCMD(TERMINAL " -e lf /") },*/
	/* { 0, XF86XK_Battery,		spawn,		SHCMD("") }, */
	/*{ 0, XF86XK_Launch1,		spawn,		SHCMD("xset dpms force off") },
	{ 0, XF86XK_TouchpadToggle,	spawn,		SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOff,	spawn,		SHCMD("synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOn,		spawn,		SHCMD("synclient TouchpadOff=0") },*/

	/* { 0, XF86XK_AudioMute,		spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") }, */
	/* { 0, XF86XK_AudioRaiseVolume,	spawn,		SHCMD("pamixer --allow-boost -i 3; kill -44 $(pidof dwmblocks)") }, */
	/* { 0, XF86XK_AudioLowerVolume,	spawn,		SHCMD("pamixer --allow-boost -d 3; kill -44 $(pidof dwmblocks)") }, */
	/* { 0, XF86XK_AudioPrev,		spawn,		SHCMD("mpc --port 6601 prev") }, */
	/* { 0, XF86XK_AudioNext,		spawn,		SHCMD("mpc --port 6601 next") }, */
	/* { 0, XF86XK_AudioPause,		spawn,		SHCMD("mpc --port 6601 pause") }, */
	/* { 0, XF86XK_AudioPlay,		spawn,		SHCMD("mpc --port 6601 play") }, */
	/* { 0, XF86XK_AudioStop,		spawn,		SHCMD("mpc --port 6601 stop") }, */
	/* { 0, XF86XK_AudioRewind,	spawn,		SHCMD("mpc --port 6601 seek -10") }, */
	/* { 0, XF86XK_AudioForward,	spawn,		SHCMD("mpc --port 6601 seek +10") }, */
	/* { 0, XF86XK_AudioMedia,		spawn,		SHCMD(TERMINAL " -e ncmpcpp") }, */
	/* { 0, XF86XK_AudioMicMute,	spawn,		SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") }, */
	/*{ MODKEY,			XK_Insert,	spawn,		SHCMD("xdotool type $(grep -v '^#' ~/.local/share/larbs/snippets | dmenu -i -l 50 | cut -d' ' -f1)") },*/

	/* { MODKEY,			XK_F12,		spawn,		SHCMD("groff -mom /usr/local/share/dwm/larbs.mom -Tpdf | zathura -") }, */
	/*{ MODKEY,			XK_F2,		spawn,		SHCMD("tutorialvids") },
	{ MODKEY,			XK_F3,		spawn,		SHCMD("displayselect") },*/
	/*{ MODKEY,			XK_F4,		spawn,		SHCMD(TERMINAL " -e pulsemixer; kill -44 $(pidof dwmblocks)") },*/
	 /*{ MODKEY,			XK_N,		xrdb,		{.v = NULL } }, */
	/*{ MODKEY,			XK_F6,		spawn,		SHCMD("torwrap") },
	{ MODKEY,			XK_F7,		spawn,		SHCMD("td-toggle") },
	{ MODKEY,			XK_F8,		spawn,		SHCMD("mw -Y") },
	{ MODKEY,			XK_F9,		spawn,		SHCMD("dmenumount") },
	{ MODKEY,			XK_F10,		spawn,		SHCMD("dmenuumount") },
	{ MODKEY,			XK_F11,		spawn,		SHCMD("mpv --no-cache --no-osc --no-input-default-bindings --profile=low-latency --input-conf=/dev/null --title=webcam $(ls /dev/video[0,2,4,6,8] | tail -n 1)") },
	{ MODKEY,			XK_F12,		spawn,		SHCMD("remaps & notify-send \\\"⌨️ Keyboard remapping...\\\" \\\"Re-running keyboard defaults for any newly plugged-in keyboards.\\\"") },*/
	/*{ 0,				XK_Print,	spawn,		SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png") },
	{ ShiftMask,			XK_Print,	spawn,		SHCMD("maimpick") },
	{ MODKEY,			XK_Print,	spawn,		SHCMD("dmenurecord") },
	{ MODKEY|ShiftMask,		XK_Print,	spawn,		SHCMD("dmenurecord kill") },
	{ MODKEY,			XK_Delete,	spawn,		SHCMD("dmenurecord kill") },*/

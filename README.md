# My build of dwm

## FAQ (WIP)

> What are the bindings?

Check out [config.h](config.h).

## Patches and features

- Clickable statusbar with my build of [dwmblocks](https://github.com/lukesmithxyz/dwmblocks).
- Can be scripted by redirecting text like this: echo "view" >> /tmp/dwm.fifo
- Reads colors from a fifo file (TODO explanation)
- New layouts: bstack, fibonacci, deck, centered master and more.
- True fullscreen (F1) and prevents focus shifting.
- Windows can be made sticky (`super+s`).
- vanitygaps: Gaps allowed across all layouts.
- swallow patch: if a program run from a terminal would make it inoperable, it temporarily takes its place to save space.
